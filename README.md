# Design Patterns Quark

Notas:
- El proyecto fue desarrollado en Unity versión 2021.1.16f1
- Se puede verificar el proyecto ejecutando la Build ubicada en \Build
- Se subió el file de proyecto de Visual Studio ubicado en la carpeta raíz, pero preferiblemente importar el proyecto y verificarlo directamente desde Unity.
- Los archivos fuente de los patrones se encuentran en \Assets\Sources\Patrones