﻿using System;

namespace DesignPatterns.TemplateMethod.Core
{
    public class CreditoPersonal : Credito
    {
        public CreditoPersonal(ClienteTM c) : base(c)
        {
        }

        protected override string VerificarAcciones()
        {
            return "No es requerido verificar acciones para un credito personal";
        }

        protected override string VerificarBalance()
        {
            return "Verificando balance bancario para un prestamo personal";
        }

        protected override string VerificarCreditos()
        {
            return "Verificando otros creditos para para un prestamo personal";
        }

        protected override string VerificarIngresos()
        {
            return "Verificando ingresos para asignar un prestamo personal";
        }
    }
}
