﻿using System;

namespace DesignPatterns.TemplateMethod.Core
{
    public class CreditoHipotecario : Credito
    {
        public CreditoHipotecario(ClienteTM c) : base(c)
        {
        }

        protected override string VerificarAcciones()
        {
            return "Verificando acciones para asignar un credito hipotecario";
        }

        protected override string VerificarBalance()
        {
            return "Verificando balance bancario para un prestamo hipotecario";
        }

        protected override string VerificarCreditos()
        {
            return "Verificando otros creditos para para un prestamo hipotecario";
        }

        protected override string VerificarIngresos()
        {
            return "Verificando ingresos para asignar un prestamo hipotecario";
        }
    }
}
