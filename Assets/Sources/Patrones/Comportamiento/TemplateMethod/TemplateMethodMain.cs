﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace DesignPatterns.TemplateMethod.Core
{
    public class TemplateMethodMain : MonoBehaviour
    {
        [Header("Template Method Attributes")]
        [SerializeField] private TMP_Dropdown _clientDropdown;
        [SerializeField] private TMP_Dropdown _creditDropdown;

        [SerializeField] private TMP_Text _console;

        private List<ClienteTM> _clientes = new List<ClienteTM>();
        private List<string> _creditos = new List<string>();

        private void Start()
        {
            SetupTemplateMethodExample();
        }

        private void SetupTemplateMethodExample()
        {
            _clientes.Add(new ClienteTM() { Nombre = "Luis Garcia" });
            _clientes.Add(new ClienteTM() { Nombre = "Jorge Lopez" });
            _clientes.Add(new ClienteTM() { Nombre = "Julian Perez"});

            List<string> options = new List<string>();
            foreach (var cliente in _clientes)
            {
                options.Add(cliente.Nombre);
            }
            _clientDropdown.ClearOptions();
            _clientDropdown.AddOptions(options);

            _creditos.Add("Personal");
            _creditos.Add("Hipotecario");

            _creditDropdown.ClearOptions();
            _creditDropdown.AddOptions(_creditos);
        }

        public void OnSolicitarBtnClick()
        {
            _console.text = string.Empty;

            string credito = _creditDropdown.options[_creditDropdown.value].text;
            ClienteTM cliente = new ClienteTM();

            foreach (var client in _clientes)
            {
                if (client.Nombre == _clientDropdown.options[_clientDropdown.value].text)
                    cliente = client;
            }

            try
            {
                if (credito == null)
                    _console.text += ($"\nDebe seleccionar un cliente");

                Credito c;
                switch (credito)
                {
                    case "Hipotecario":
                        c = new CreditoHipotecario(cliente);
                        break;
                    case "Personal":
                        c = new CreditoPersonal(cliente);
                        break;
                    default:
                        throw new Exception("\nDebe seleccionar un crédito");
                }

                string[] verificacion = c.Verificar();
                foreach (var text in verificacion)
                {
                    _console.text += $"\n{text}";
                }
            }
            catch (Exception ee)
            {
                _console.text = ee.Message;
            }
        }
    }
}
