﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace DesignPatterns.Memento.Core
{
    public class MementoMain : MonoBehaviour
    {
        [SerializeField] private TMP_Text _console;

        private static CareTaker _caretaker = new CareTaker();

        public void RunMementoExample()
        {
            _console.text = string.Empty;

            var p = new Persona();

            p.Nombre = "Pepe";
            _console.text += ($"\nOriginator: Guardando Memento para {p.Nombre}");
            _caretaker.Add(p.saveToMemento());

            p.Nombre = "Pepe1";
            _console.text += ($"\nOriginator: Guardando Memento para {p.Nombre}");
            _caretaker.Add(p.saveToMemento());

            p.Nombre = "Pepe2";
            _console.text += ($"\nOriginator: Guardando Memento para {p.Nombre}");
            _caretaker.Add(p.saveToMemento());

            _console.text += ($"\nViendo memento 1: {_caretaker.GetMemento(0).Estado}");
            _console.text += ($"\nViendo memento 2: {_caretaker.GetMemento(1).Estado}");
            _console.text += ($"\nViendo memento 3: {_caretaker.GetMemento(2).Estado}");

            Memento mem = _caretaker.GetMemento(0);
            p.restoreToMemento(mem);
            _console.text += ($"\nOriginator: Recuperado memento {p.Nombre}");

            mem = _caretaker.GetMemento(2);
            p.restoreToMemento(mem);
            _console.text += ($"\nOriginator: Recuperado memento {p.Nombre}");
        }
    }
}
