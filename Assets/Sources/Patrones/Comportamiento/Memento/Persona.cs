﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Memento.Core
{
    public class Persona
    {
        public string Nombre { get; set; }

        public Memento saveToMemento()
        {

            return new Memento(Nombre);
        }

        public void restoreToMemento(Memento m)
        {
            Nombre = m.Estado;
        }
    }
}
