﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Iterator.Core
{
    public class Item
    {
        public string Nombre { get; set; }
        public Item (string nom)
        {
            Nombre = nom;
        }

        public override string ToString()
        {
            return Nombre;
        }
    }
}
