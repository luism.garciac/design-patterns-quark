﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace DesignPatterns.Iterator.Core
{
    public class IteratorMain : MonoBehaviour
    {
        [Header("Iterator Elements")]

        [SerializeField] private TMP_InputField _iteratorInputItem;
        [SerializeField] private TMP_Text _iteratorTextList;
        [SerializeField] private TMP_InputField _iteratorTextPrimero;
        [SerializeField] private TMP_InputField _iteratorTextActual;

        Collection _collection = new ConcreteCollection();
        IteratorEjemplo _iterador;

        public void Start()
        {
            SetupIteratorExample();
        }

        public void SetupIteratorExample()
        {
            _iterador = _collection.CreateIterator();
        }

        private void Listar()
        {
            _iteratorTextList.text = "";
            _iterador.First();

            Item theItem = _iterador.CurrentItem();

            if (theItem != null)
                _iteratorTextList.text = JsonConvert.SerializeObject(theItem);

            while (!_iterador.IsDone())
            {
                _iterador.Siguiente();
                _iteratorTextList.text += $"\n{JsonConvert.SerializeObject(_iterador.CurrentItem())}";
            }
        }

        public void IteratorAgregar()
        {
            var item = new Item(_iteratorInputItem.text);
            _collection.Agregar(item);
            Listar();
        }

        public void IteratorPrimero()
        {
            _iterador.First();
            _iteratorTextPrimero.text = _iterador.CurrentItem().Nombre;
            _iteratorTextActual.text = _iterador.CurrentItem().Nombre;

        }

        public void IteratorSiguiente()
        {
            if (_iterador.IsDone() == false)
            {
                _iterador.Siguiente();
                _iteratorTextActual.text = _iterador.CurrentItem().Nombre;
            }
        }
    }
}
