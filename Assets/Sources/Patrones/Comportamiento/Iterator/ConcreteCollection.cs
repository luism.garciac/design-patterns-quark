﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Iterator.Core
{
    public class ConcreteCollection : Collection
    {
        private List<Item> _listaItem = new List<Item>();
        private Item _item;
        private int _posicion;

        public override IteratorEjemplo CreateIterator()
        {
            return new ConcreteIteratorEjemplo(this);
        }

        public override void Agregar(Item item)
        {
            _listaItem.Add(item);
        }

        public override Item Index(int index)
        {
            _item = _listaItem[index];
            _posicion = index;

            return _item;
        }

        public override int Count()
        {
            return _listaItem.Count;
        }

        public override int Posicion()
        {
            return _posicion;
        }
    }
}
