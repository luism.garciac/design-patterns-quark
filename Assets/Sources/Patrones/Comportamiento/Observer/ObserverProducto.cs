﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace DesignPatterns.Observer.Core
{
    public class ObserverProducto : ISujetoProducto
    {
        private List<IObserverUsuario> _usuarios = new List<IObserverUsuario>();

        public string Nombre { get; set; }
        private string _precio;

        public string Precio 
        {
            get
            {
                return _precio;
            }
        }

        public string SetPrecio(string value)
        {
            _precio = value;
            return this.Notificar();
        }

        public ObserverProducto(string nombre, string precio)
        {
            Nombre = nombre;
            _precio = precio;
        }

        public string Agregar(IObserverUsuario usuario)
        {
            if (!_usuarios.Contains(usuario))
            {
                _usuarios.Add(usuario);

                return $"\nUsuario {((ObserverUsuario)usuario).Nombre} suscripto";
            }
            else
            {
                return $"\nYa existe suscripcion para {((ObserverUsuario)usuario).Nombre}";
            }
        }

        public string Notificar()
        {
            string notificacion = string.Empty;

            foreach (var usuario in _usuarios)
            {
                notificacion += usuario.Actualizar(this);
            }

            if (notificacion != string.Empty)
                return notificacion;

            if (_usuarios.Count == 0)
            {
                return $"\nNo existen suscriptores para el producto";
            }

            return string.Empty;
        }

        public string Quitar(IObserverUsuario usuario)
        {
            if (_usuarios.Contains(usuario))
            {
                _usuarios.Remove(usuario);
                return $"\nUsuario {((ObserverUsuario)usuario).Nombre} desuscripto";
            }
            else
            {
                return $"\nNo existe suscripcion para {((ObserverUsuario)usuario).Nombre}";
            }
        }
    }
}
