﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace DesignPatterns.Observer.Core
{
    public class ObserverUsuario : IObserverUsuario
    {
        public ObserverUsuario(string nombre)
        {
            Nombre = nombre;
        }
        
        public string Nombre { get; set; }

        public string Actualizar(ObserverProducto p)
        {
            return $"\nEl usuario {Nombre} recibio la notificacion de cambio de precio de {p.Nombre}";
        }
    }
}
