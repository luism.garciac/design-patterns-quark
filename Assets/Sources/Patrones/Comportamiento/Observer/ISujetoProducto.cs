﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Observer.Core
{
    public interface ISujetoProducto
    {
        public string Agregar(IObserverUsuario usuario);
        public string Quitar(IObserverUsuario usuario);
        public string Notificar();
    }
}
