﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace DesignPatterns.Observer.Core
{
    public class ObserverMain : MonoBehaviour
    {
        [Header("Observer Attributes")]
        [SerializeField] private TMP_Dropdown _observerDropdown;
        [SerializeField] private TMP_InputField _price;
        [SerializeField] private TMP_Text _console;

        private void Start()
        {
            SetupObserverExample();
        }

        private void SetupObserverExample()
        {
            List<string> options = new List<string>();

            foreach (var usuario in _usuarios)
            {
                options.Add(usuario.Nombre);
            }

            _observerDropdown.ClearOptions();
            _observerDropdown.AddOptions(options);
            _price.onSubmit.RemoveAllListeners();
            _price.onEndEdit.AddListener(OnPriceChanged);
            _price.text = _producto.Precio;
        }

        private ObserverProducto _producto = new ObserverProducto("Gaseosa", "500");

        private List<ObserverUsuario> _usuarios = new List<ObserverUsuario>
        {
            new ObserverUsuario("Carlos"), new ObserverUsuario("Juan"), new ObserverUsuario("Pedro"),
        };

        private void OnPriceChanged(string precio)
        {
            if (precio != _producto.Precio)
                _console.text += _producto.SetPrecio(precio);
        }

        public void OnSubscribeButtonClick()
        {
            _console.text += _producto.Agregar(_usuarios[_observerDropdown.value]);
        }

        public void OnUnsuscribeButtonClick()
        {
            _console.text += _producto.Quitar(_usuarios[_observerDropdown.value]);
        }
    }
}
