﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.StrategyPattern.Core
{
    public class RifleStrategy : ArmaStrategy
    {
        public override string Disparar()
        {
            return ("\nRatatatata!");
        }

        public override string GetName()
        {
            return ("Rifle");
        }
    }
}
