﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.StrategyPattern.Core
{
    public class BazookaStrategy : ArmaStrategy
    {
        public override string Disparar()
        {
            return ("\nBoooom!");
        }
        public override string GetName()
        {
            return ("Bazooka");
        }
    }
}
