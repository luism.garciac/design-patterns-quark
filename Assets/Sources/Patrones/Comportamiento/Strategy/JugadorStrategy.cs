﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.StrategyPattern.Core
{
    public class JugadorStrategy
    {
        ArmaStrategy _estrategia;

        public string CambiarEstrategia(ArmaStrategy estrategia)
        {
            if (estrategia == null) 
                return $"\nLa estrategia de disparo no puede ser realizada";
            
            _estrategia = estrategia;

            return $"\nEstrategia actual: {_estrategia.GetName()}";
        }

        public string Disparar()
        {
            if (_estrategia == null)
                return ("\nArma no disponible");
            return _estrategia.Disparar();
        }
    }
}
