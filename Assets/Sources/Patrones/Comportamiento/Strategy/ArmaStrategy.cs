﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.StrategyPattern.Core
{
    public abstract class ArmaStrategy
    {
        public abstract string Disparar();
        public abstract string GetName();
    }
}
