﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.StrategyPattern.Core
{
    public class PistolaStrategy : ArmaStrategy
    {
        public override string Disparar()
        {
            return ("\nPium pium!");
        }
        public override string GetName()
        {
            return ("Pistola");
        }
    }
}