﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace DesignPatterns.StrategyPattern.Core
{
    public class StrategyMain : MonoBehaviour
    {
        [Header("Strategy Attributes")]
        [SerializeField] private TMP_Dropdown _strategyDropdown;
        [SerializeField] private TMP_Text _console;

        private JugadorStrategy _jugador = new JugadorStrategy();
        private List<ArmaStrategy> _estrategiasDisparo = new List<ArmaStrategy>();

        private void Start()
        {
            SetupStrategyExample();
        }

        private void SetupStrategyExample()
        {
            _estrategiasDisparo.Add(new PistolaStrategy());
            _estrategiasDisparo.Add(new RifleStrategy());
            _estrategiasDisparo.Add(new BazookaStrategy());

            List<string> options = new List<string>();

            foreach (var estrategia in _estrategiasDisparo)
            {
                options.Add(estrategia.GetName());
            }

            _strategyDropdown.ClearOptions();
            _strategyDropdown.AddOptions(options);
        }

        public void StrategyOnDispararClick()
        {
            _console.text += _jugador.Disparar();
        }

        public void StrategyOnSeleccionarArma()
        {
            foreach (var estrategia in _estrategiasDisparo)
            {
                if (estrategia.GetName() == _strategyDropdown.options[_strategyDropdown.value].text)
                    _console.text += _jugador.CambiarEstrategia(estrategia);
            }
        }
    }
}
