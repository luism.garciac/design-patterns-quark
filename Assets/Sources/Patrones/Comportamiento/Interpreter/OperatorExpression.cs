﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Interpreter.Core
{
    public class OperatorExpression : IExpression
    {
        private string _operation;
        public OperatorExpression(string token)
        {
            _operation = token;
        }

        public void Interpret(Context context)
        {
            context.SetOperation(_operation);
        }
    }
}
