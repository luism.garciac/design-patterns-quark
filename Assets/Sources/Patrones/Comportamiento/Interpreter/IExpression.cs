﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Interpreter.Core
{
    interface IExpression
    {
        void Interpret(Context context);
    }
}
