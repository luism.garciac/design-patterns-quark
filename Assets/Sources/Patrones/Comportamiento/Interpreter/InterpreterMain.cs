﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace DesignPatterns.Interpreter.Core
{
    public class InterpreterMain : MonoBehaviour
    {

        [Header("Interpreter Elements")]
        [SerializeField] private TMP_InputField _textOperation;
        [SerializeField] private TMP_Text _console;

        public void RunInterpreterExample()
        {
            _console.text = string.Empty;

            string[] tree;
            var context = new Context();
            var expression = new List<IExpression>();

            string val = _textOperation.text;
            val = val.ToLower();
            tree = val.Split(' ');

            IExpression exp;

            foreach (var t in tree)
            {
                if (t == "mas" || t == "menos")
                    exp = new OperatorExpression(t);
                else
                    exp = new NumericExpression(t);

                exp.Interpret(context);
            }

            _console.text = ($"El resultado para:\n\n{_textOperation.text} \n\nes: {context.GetResult()}");
        }
    }
}
