﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace DesignPatterns.Command.Core
{
    public class CommandMain : MonoBehaviour
    {
        [SerializeField] private TMP_Text _console;

        public void RunCommandExample()
        {
            var empresa = new EmpresaInvoker();
            var producto = new ProductoReciever();

            producto.Cantidad = 100;

            _console.text += $"\n\nStock Inicial: {producto.Cantidad}";

            var ordenAlta = new AltaStockCommand(producto, 10);
            empresa.TomarOrden(ordenAlta);

            var ordenBaja = new BajaStockCommand(producto, 50);
            empresa.TomarOrden(ordenBaja);

            string result = empresa.ProcesarOrdenes();

            _console.text += $"\n\n{result}";
            _console.text += $"\n\nStock Final: {producto.Cantidad}";
        }
    }
}
