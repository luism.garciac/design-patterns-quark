﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesignPatterns.Command.Core
{
    public abstract class OrdenCommand
    {
        public abstract string Ejecutar();
        protected ProductoReciever _producto;

        protected double _cantidad;

        protected OrdenCommand(ProductoReciever producto, double cantidad)
        {
            _producto = producto;
            _cantidad = cantidad;
        }
    }
}


