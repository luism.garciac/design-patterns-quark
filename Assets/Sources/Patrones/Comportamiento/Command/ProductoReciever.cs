﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesignPatterns.Command.Core
{
    public class ProductoReciever
    {
        public double Cantidad { get; set; }
        public string Nombre { get; set; }

        public string SumarStock(double cant)
        {
            Cantidad = Cantidad + cant;
            return $"\n\nSumando {cant} unidades";
        }

        public string RestarStock(double cant)
        {
            Cantidad = Cantidad - cant;
            return $"\n\nRestando {cant} unidades";
        }
    }
}


