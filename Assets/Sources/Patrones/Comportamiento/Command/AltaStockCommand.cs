﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesignPatterns.Command.Core
{
    public class AltaStockCommand : OrdenCommand
    {
        public AltaStockCommand(ProductoReciever producto, double cantidad) : base(producto, cantidad)
        {
        }

        public override string Ejecutar()
        {
            return _producto.SumarStock(_cantidad);
        }
    }
}


