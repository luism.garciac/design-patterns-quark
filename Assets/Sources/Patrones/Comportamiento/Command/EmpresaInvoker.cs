﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesignPatterns.Command.Core
{
    public class EmpresaInvoker
    {
        private List<OrdenCommand> ordenes = new List<OrdenCommand>();

        public void TomarOrden(OrdenCommand cmd)
        {
            ordenes.Add(cmd);
        }

        public string ProcesarOrdenes()
        {
            string result = string.Empty;
            foreach (var orden in ordenes)
            {
                result = result + orden.Ejecutar();
            }
            ordenes.Clear();
            return result;
        }
    }
}


