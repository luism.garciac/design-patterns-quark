﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesignPatterns.Command.Core
{
    public class BajaStockCommand : OrdenCommand
    {
        public BajaStockCommand(ProductoReciever producto, double cantidad) : base(producto, cantidad)
        {
        }

        public override string Ejecutar()
        {
            return _producto.RestarStock(_cantidad);
        }
    }
}


