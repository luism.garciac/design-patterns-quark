using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesignPatterns.Mediator.Core
{
    public abstract class UsuarioMediator 
    {
        List<Mensaje> _mensajes;
        string _nombre;

        public UsuarioMediator (string nombre)
        {
            _nombre = nombre;
            _mensajes = new List<Mensaje>();
        }

        public Mensaje[] Mensajes
        {
            get 
            {
                return _mensajes.ToArray();
            }
        }

        public string Nombre
        {
            get
            {
                return _nombre;
            }
        }

        public override string ToString()
        {
            return _nombre;
        }

        public ChatMediator Chat { get; set; }

        public void Enviar(string mensaje, UsuarioMediator para)
        {
            Chat.Enviar(mensaje, para, this);
        }

        public void Recibir (Mensaje msg)
        {
            _mensajes.Add(msg);
        }

    }

}
