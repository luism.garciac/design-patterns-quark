﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Mediator.Core
{
    public class Mensaje
    {
        private DateTime _fecha;

        public Mensaje()
        {
            _fecha = DateTime.Now;
        }

        public UsuarioMediator De { get; set; }
        public UsuarioMediator Para { get; set; }
        public string Texto { get; set; }

        public DateTime Fecha { get { return _fecha; } }

        public override string ToString()
        {
            return $"{Fecha.ToShortTimeString()}: de {De.Nombre} para {Para.Nombre}:\n{Texto}";
        }
    }
}
