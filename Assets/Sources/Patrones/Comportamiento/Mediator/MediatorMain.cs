﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace DesignPatterns.Mediator.Core
{
    public class MediatorMain : MonoBehaviour
    {
        [Header("Mediator Elements")]

        [SerializeField] private TMP_InputField _mediatorUser1Input;
        [SerializeField] private TMP_InputField _mediatorUser2Input;
        [SerializeField] private TMP_Text _mediatorUser1ChatZone;
        [SerializeField] private TMP_Text _mediatorUser2ChatZone;
        [SerializeField] private TMP_Text _mediatorCommonChatZone;

        ChatMediator _chat;
        UsuarioMediator u1;
        UsuarioMediator u2;

        private void Start()
        {
            SetupMediatorExample();
        }
        public void SetupMediatorExample()
        {
            _chat = new ChatRoom();
            u1 = new UsuarioMediator1("Usuario1");
            u2 = new UsuarioMediator2("Usuario2");
            _chat.Registrar(u1);
            _chat.Registrar(u2);
        }
        public void MediatorOnUser1SendBtnClick()
        {
            _chat.Enviar(_mediatorUser1Input.text, u2, u1);
            MostrarMensajes();
            MostrarMensajesU2();
        }
        public void MediatorOnUser2SendBtnClick()
        {
            _chat.Enviar(_mediatorUser2Input.text, u1, u2);
            MostrarMensajes();
            MostrarMensajesU1();
        }
        private void MostrarMensajesU1()
        {
            _mediatorUser1ChatZone.text = string.Empty;

            foreach (var mmsg in u1.Mensajes)
            {
                _mediatorUser1ChatZone.text += $"\n {mmsg.ToString()}";
            }
        }
        private void MostrarMensajesU2()
        {
            _mediatorUser2ChatZone.text = string.Empty;

            foreach (var mmsg in u2.Mensajes)
            {
                _mediatorUser2ChatZone.text += $"\n {mmsg.ToString()}";
            }
        }
        private void MostrarMensajes()
        {
            _mediatorCommonChatZone.text = string.Empty;

            foreach (var mmsg in _chat.Mensajes)
            {
                _mediatorCommonChatZone.text += $"\n {mmsg.ToString()}";
            }
        }
    }
}
