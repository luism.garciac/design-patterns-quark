﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Mediator.Core
{
    public abstract class ChatMediator
    {
        protected IList<Mensaje> _mensajes;

        protected ChatMediator()
        {
            _mensajes = new List<Mensaje>();
        }

        public Mensaje[] Mensajes
        {
            get 
            {
                return _mensajes.ToArray();
            }
        }

        public abstract void Enviar(string mensaje, UsuarioMediator para, UsuarioMediator de);
        public abstract void Registrar(UsuarioMediator usuario);
    }
}
