﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Mediator.Core
{
    public class ChatRoom : ChatMediator
    {
        IDictionary<string, UsuarioMediator> _participantes;

        public ChatRoom()
        {
            _participantes = new Dictionary<string, UsuarioMediator>();
        }

        public override void Enviar(string mensaje, UsuarioMediator para, UsuarioMediator de)
        {
            Mensaje msg = new Mensaje();
            msg.De = de;
            msg.Para = para;
            msg.Texto = mensaje;

            if (_participantes.ContainsKey(para.Nombre))
            {
                _participantes[para.Nombre].Recibir(msg);
                _mensajes.Add(msg);
            }
        }

        public override void Registrar(UsuarioMediator usuario)
        {
            if (!_participantes.ContainsKey(usuario.Nombre))
            {
                _participantes.Add(usuario.Nombre, usuario);
            }
        }
    }
}
