﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.ChainOfResponsibility.Core
{
    public class Director : Aprobador
    {
        public override string Procesar(Compra c)
        {
            return $"\n\nCompra aprobada por el {this.GetType().Name}";
        }
    }
}
