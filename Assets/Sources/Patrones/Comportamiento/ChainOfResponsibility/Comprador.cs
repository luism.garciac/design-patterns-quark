﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.ChainOfResponsibility.Core
{
    public class Comprador : Aprobador
    {
        public override string Procesar(Compra c)
        {
            if (c.Importe < 100)
                return $"\n\nCompra aprobada por el {this.GetType().Name}";
            else
                return _siguiente.Procesar(c);
        }
    }
}
