﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace DesignPatterns.ChainOfResponsibility.Core
{
    public class ChainORMain : MonoBehaviour
    {
        [Header("Chain OR Elements")]

        [SerializeField] private TMP_Text _console;
        [SerializeField] private TMP_InputField _montoCompra;

        public void RunChainORExample()
        {
            _console.text = string.Empty;

            var comprador = new Comprador();
            var gerente = new Gerente();
            var director = new Director();

            comprador.AgregarSiguiente(gerente);
            gerente.AgregarSiguiente(director);

            var c = new Compra();

            if (string.IsNullOrEmpty(_montoCompra.text))
                c.Importe = 0;
            else
                c.Importe = int.Parse(_montoCompra.text);

            _console.text += $"{comprador.Procesar(c)}";
        }
    }
}
