﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Visitor.Core
{
    public abstract class VisitorComponente
    {
        string _serial;
        string _descripcion;

        protected VisitorComponente(string serial, string descripcion)
        {
            _serial = serial;
            _descripcion = descripcion;
        }

        public string Serial { get => _serial;}
        public string Descripcion { get => _descripcion;}

        public abstract string Aceptar(IVisitor visitor);
    }

}
