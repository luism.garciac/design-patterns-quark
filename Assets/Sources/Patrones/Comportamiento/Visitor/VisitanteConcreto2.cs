﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Visitor.Core
{
    public class VisitanteConcreto2 : IVisitor
    {
        public string Visitar(Procesador componente)
        {
            return $"\nVisitanteConcreto2: {componente.Descripcion}";
        }

        public string Visitar(PlacaBase componente)
        {
            return $"\nVisitanteConcreto2: {componente.Descripcion}";
        }

        public string Visitar(DiscoRigido componente)
        {
            return $"\nVisitanteConcreto2: {componente.Descripcion}";
        }
    }
}
