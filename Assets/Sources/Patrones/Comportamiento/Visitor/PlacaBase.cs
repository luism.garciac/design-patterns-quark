﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Visitor.Core
{
    public class PlacaBase : VisitorComponente
    {
        public PlacaBase(string serial, string descripcion) : base(serial, descripcion)
        {
        }

        public override string Aceptar(IVisitor visitor)
        {
            return visitor.Visitar(this);
        }
    }
}
