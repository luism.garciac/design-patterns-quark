﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Visitor.Core
{
    public class VisitanteConcreto : IVisitor
    {
        public string Visitar(Procesador componente)
        {
            return $"\nVisitante Concreto 1: Procesador s/n: {componente.Serial}";
        }

        public string Visitar(PlacaBase componente)
        {
            return $"\nVisitante Concreto 1: Placa Base s/n: {componente.Serial}";
        }

        public string Visitar(DiscoRigido componente)
        {
            return $"\nVisitante Concreto 1: Disco Rigido s/n: {componente.Serial}";
        }
    }
}
