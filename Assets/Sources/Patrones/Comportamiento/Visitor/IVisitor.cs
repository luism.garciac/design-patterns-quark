﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Visitor.Core
{
    public interface IVisitor
    {
        string Visitar(Procesador componente);
        string Visitar(PlacaBase componente);
        string Visitar(DiscoRigido componente);
    }
}
