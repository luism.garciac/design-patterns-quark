﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace DesignPatterns.Visitor.Core
{
    public class VisitorMain : MonoBehaviour
    {
        [SerializeField] private TMP_Text _console;

        public void RunVisitorExample()
        {

            _console.text = string.Empty;

            VisitorComponente procesador = new Procesador("KWDWQ-1245", "Procesador Core I3");
            VisitorComponente placaBase = new PlacaBase("19-!SAFBAWIIF-22", "Placa Base Asus AWK-54");
            VisitorComponente discoRigido = new DiscoRigido("1lkfkf1-KWD11", "Disco Rigido WD 1TB");

            IVisitor visitante = new VisitanteConcreto();

            _console.text += ($"{procesador.Aceptar(visitante)}");
            _console.text += ($"{placaBase.Aceptar(visitante)}");
            _console.text += ($"{discoRigido.Aceptar(visitante)}");

            IVisitor visitante2 = new VisitanteConcreto2();

            _console.text += ($"{procesador.Aceptar(visitante2)}");
            _console.text += ($"{placaBase.Aceptar(visitante2)}");
            _console.text += ($"{discoRigido.Aceptar(visitante2)}");

        }
    }
}
