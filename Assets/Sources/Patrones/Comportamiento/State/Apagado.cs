﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.State.Core
{
    class Apagado : Estado
    {
        public override void ControlarEstado(StateSwitch sw)
        {
            sw.DefinirEstado(new Encendido());
        }

        public override string Describir()
        {
            return "Switch Apagado";
        }
    }
}
