﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.State.Core
{
    class Encendido : Estado
    {
        public override void ControlarEstado(StateSwitch sw)
        {
            sw.DefinirEstado(new Apagado());
        }

        public override string Describir()
        {
            return "Switch Encendido";
        }
    }
}
