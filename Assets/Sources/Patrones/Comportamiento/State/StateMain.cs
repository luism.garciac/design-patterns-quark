﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace DesignPatterns.State.Core
{
    public class StateMain : MonoBehaviour
    {
        [SerializeField] private TMP_Text _console;

        public void RunStateExample()
        {
            StateSwitch _switch = new StateSwitch();

            _console.text += _switch.Presionar();
            _console.text += _switch.Presionar();
            _console.text += _switch.Presionar();
            _console.text += _switch.Presionar();
            _console.text += _switch.Presionar();
            _console.text += _switch.Presionar();
        }
    }
}
