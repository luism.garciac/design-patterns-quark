using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesignPatterns.State.Core
{
    public class StateSwitch
    {
        Estado _estado;

        public StateSwitch()
        {
            _estado = new Encendido();
        }

        public void DefinirEstado(Estado estado)
        {
            _estado = estado;
        }

        public string Presionar()
        {
            _estado.ControlarEstado(this);
            return ($"\n{_estado.Describir()}");
        }
    }
}
