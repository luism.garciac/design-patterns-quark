﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.State.Core
{
    public abstract class Estado
    {
        public abstract void ControlarEstado(StateSwitch sw);
        public abstract string Describir();
    }
}
