using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesignPatterns.Singleton.Core
{
    public class Usuario
    {
        public string Username { get; set; }
        public string Password { get; set; }

    }
}


