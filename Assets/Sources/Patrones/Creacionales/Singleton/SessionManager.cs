using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesignPatterns.Singleton.Core
{
    public class SessionManager
    {
        private SessionManager()
        {

        }

        public Usuario Usuario { get; set; }
        public DateTime FechaInicio { get; set; }

        public static SessionManager _session;

        public static SessionManager GetInstance
        {
            get
            {
                if (_session == null) throw new Exception("\n\nSesion no iniciada");
                    return _session;
            }
        }

        //Para multihilo
        private static object _lock = new System.Object();

        //Logica 1 a implementar mediante el Singleton
        public static string Login(Usuario usuario)
        {
            lock (_lock)
            {
                if (_session == null)
                {
                    _session = new SessionManager();
                    _session.Usuario = usuario;
                    _session.FechaInicio = DateTime.Now;

                    return "\n\nSesion iniciada correctamente";
                }
                else
                {
                    return "\n\nSesion ya iniciada";
                }
            }


        }

        //Logica 2 a implementar mediante el Singleton
        public static string Logout()
        {
            lock (_lock)
            {
                if (_session != null)
                {
                    _session = null;
                    return "\n\nSesion terminada correctamente";
                }
                else
                {
                    return "\n\nSesion no iniciada";
                }
            }
        }

        public static string GetUsuario()
        {
            return $"\n\nUsuario {_session.Usuario.Username} se logueo en {_session.FechaInicio}";
        }

    }
}


