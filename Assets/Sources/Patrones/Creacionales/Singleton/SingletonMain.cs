﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace DesignPatterns.Singleton.Core
{
    public class SingletonMain : MonoBehaviour
    {
        [SerializeField] private TMP_Text _console;

        public void RunSingletonExample()
        {
            _console.text = string.Empty;

            Usuario usuario = new Usuario();
            usuario.Username = "Luis";
            usuario.Password = "pass";

            _console.text += SessionManager.Login(usuario);

            try
            {
                _console.text += $"\n\nEl usuario logueado es: {SessionManager.GetInstance.Usuario.Username}";

            }
            catch (System.Exception e)
            {
                _console.text += e.Message;
            }

            _console.text += SessionManager.GetUsuario();

            _console.text += SessionManager.Logout();
        }
    }
}
