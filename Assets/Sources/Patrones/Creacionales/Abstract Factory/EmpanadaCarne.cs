﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory.Core
{
    public class EmpanadaCarne : Empanada
    {
        public EmpanadaCarne()
        {
            _descripcion = "Empanada de carne";
        }
    }
}
