﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory.Core
{
    public class PizzaCancha : Pizza
    {
        public PizzaCancha()
        {
            _descripcion = "Pizza de cancha";
        }
    }
}
