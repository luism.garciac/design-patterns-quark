﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory.Core
{
    public abstract class Empanada
    {
        protected string _descripcion;

        public object Descripcion { get => _descripcion; }
    }
}
