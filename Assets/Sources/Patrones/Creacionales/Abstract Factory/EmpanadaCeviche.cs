﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory.Core
{
    public class EmpanadaCeviche : Empanada
    {
        public EmpanadaCeviche()
        {
            _descripcion = "Empanada de ceviche";
        }
    }
}
