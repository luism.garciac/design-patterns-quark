﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.AbstractFactory.Core
{
    public class PizzeriaArgentina : Pizzeria
    {
        public override Pizza CrearPizza()
        {
            return new PizzaCancha();
        }

        public override Empanada CrearEmpanada()
        {
            return new EmpanadaCarne();
        }
    }
}
