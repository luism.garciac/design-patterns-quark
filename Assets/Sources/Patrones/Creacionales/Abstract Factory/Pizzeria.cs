using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesignPatterns.AbstractFactory.Core
{
    public abstract class Pizzeria
    {
        public abstract Pizza CrearPizza();
        public abstract Empanada CrearEmpanada();

    }
}


