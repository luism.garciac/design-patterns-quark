﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace DesignPatterns.AbstractFactory.Core
{
    public class AbstractFactoryMain : MonoBehaviour
    {
        [SerializeField] private TMP_Text _console;

        public void RunAbstractFactoryExample()
        {
            _console.text = string.Empty;

            Pizzeria fabrica;

            fabrica = new PizzeriaArgentina();
            Pizza pizza = fabrica.CrearPizza();
            Empanada empanada = fabrica.CrearEmpanada();
            _console.text += $"Pizzeria argentina: {pizza.Descripcion} ; Empanada: {empanada.Descripcion}";

            fabrica = new PizzeriaItaliana();
            pizza = fabrica.CrearPizza();
            empanada = fabrica.CrearEmpanada();
            _console.text += $"\n\nPizzeria Italiana: {pizza.Descripcion} ; Empanada: {empanada.Descripcion}";

            fabrica = new PizzeriaChina();
            pizza = fabrica.CrearPizza();
            empanada = fabrica.CrearEmpanada();
            _console.text += $"\n\nPizzeria China: {pizza.Descripcion} ; Empanada: {empanada.Descripcion}";
        }
    }
}
