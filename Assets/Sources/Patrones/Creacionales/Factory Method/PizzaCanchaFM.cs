﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.FactoryMethod.Core
{
    public class PizzaCanchaFM : PizzaFM
    {
        public PizzaCanchaFM(string origen)
        {
            _descripcion = "Pizza de cancha";
            _origen = origen;
        }
    }
}
