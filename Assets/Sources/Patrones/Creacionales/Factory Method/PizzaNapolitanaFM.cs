﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.FactoryMethod.Core
{
    public class PizzaNapolitanaFM : PizzaFM
    {
        public PizzaNapolitanaFM(string origen)
        {
            _descripcion = "Pizza napolitana";
            _origen = origen;
        }
    }
}
