using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesignPatterns.FactoryMethod.Core
{
    public abstract class PizzeriaFM
    {
        public abstract PizzaFM CrearPizza(string tipo);
    }
}


