﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.FactoryMethod.Core
{
    public abstract class PizzaFM
    {
        protected string _descripcion;
        protected string _origen;

        public string Descripcion()
        {
            return $"\n\nPizza de {_descripcion} hecha en {_origen}";
        }
    }
}
