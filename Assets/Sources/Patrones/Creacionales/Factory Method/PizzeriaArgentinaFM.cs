﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.FactoryMethod.Core
{
    public class PizzeriaArgentinaFM : PizzeriaFM
    {
        public override PizzaFM CrearPizza(string tipo)
        {
            if (tipo == "cancha")
            {
                return new PizzaCanchaFM("Argentina");
            }
            else if (tipo == "napo")
            {
                return new PizzaNapolitanaFM("Argentina");
            }
            else
            {
                return null;
            }
        }
    }
}
