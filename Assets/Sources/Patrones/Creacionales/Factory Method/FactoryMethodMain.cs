﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace DesignPatterns.FactoryMethod.Core
{
    public class FactoryMethodMain : MonoBehaviour
    {
        [SerializeField] private TMP_Text _console;

        public void RunFactoryMethodExample()
        {
            _console.text = string.Empty;

            PizzeriaFM pizzeria;
            PizzaFM pizza;

            pizzeria = new PizzeriaArgentinaFM();
            pizza = pizzeria.CrearPizza("napo");
            _console.text += pizza.Descripcion();

            pizza = pizzeria.CrearPizza("cancha");
            _console.text += pizza.Descripcion();

            pizzeria = new PizzeriaItalianaFM();
            pizza = pizzeria.CrearPizza("napo");
            _console.text += pizza.Descripcion();

            pizza = pizzeria.CrearPizza("cancha");
            _console.text += pizza.Descripcion();
        }
    }
}
