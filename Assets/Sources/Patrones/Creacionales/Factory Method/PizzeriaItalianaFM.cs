﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.FactoryMethod.Core
{
    public class PizzeriaItalianaFM : PizzeriaFM
    {
        public override PizzaFM CrearPizza(string tipo)
        {
            if (tipo == "cancha")
            {
                return new PizzaCanchaFM("Italia");
            }
            else if (tipo == "napo")
            {
                return new PizzaNapolitanaFM("Italia");
            }
            else
            {
                return null;
            }
        }
    }
}
