namespace DesignPatterns.Prototype.Core

{
    public class FiatPrototype : AutoPrototype
    {
        public override AutoPrototype Clonar()
        {
            return (FiatPrototype)this.MemberwiseClone();
        }

        public override string VerAuto()
        {
            return $"\n\nFiat {_modelo} color {_color}";
        }
    }

}
