namespace DesignPatterns.Prototype.Core

{
    public class AlfaRomeoPrototype : AutoPrototype
    {
        public override AutoPrototype Clonar()
        {
            return (AlfaRomeoPrototype)this.MemberwiseClone();
        }

        public override string VerAuto()
        {
            return $"\n\nAlfa Romeo {_modelo} color {_color}";
        }
    }

}
