﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace DesignPatterns.Prototype.Core
{
    public class PrototypeMain : MonoBehaviour
    {
        [SerializeField] private TMP_Text _console;

        public void RunPrototypeExample()
        {
            _console.text = string.Empty;

            AutoPrototype prototipoFiat = new FiatPrototype();
            AutoPrototype prototipoDS = new DSPrototype();
            AutoPrototype prototipoAlfaRomeo = new AlfaRomeoPrototype();

            AutoPrototype fiatPalio = prototipoFiat.Clonar();
            fiatPalio.Modelo = "Palio Fire";
            fiatPalio.Color = "Negro";
            _console.text += fiatPalio.VerAuto();

            AutoPrototype fiatUno = prototipoFiat.Clonar();
            fiatUno.Modelo = "Uno SRC";
            fiatUno.Color = "Rojo";
            _console.text += fiatUno.VerAuto();

            AutoPrototype ds3 = prototipoDS.Clonar();
            ds3.Modelo = "3 Chic";
            ds3.Color = "Gris";
            _console.text += ds3.VerAuto();

            AutoPrototype ds4 = prototipoDS.Clonar();
            ds4.Modelo = "4 Top";
            ds4.Color = "Azul";
            _console.text += ds4.VerAuto();

            AutoPrototype alfa145 = prototipoAlfaRomeo.Clonar();
            alfa145.Modelo = "145";
            alfa145.Color = "Rojo";
            _console.text += alfa145.VerAuto();

            AutoPrototype alfa146 = prototipoAlfaRomeo.Clonar();
            alfa146.Modelo = "146";
            alfa146.Color = "Blanco";
            _console.text += alfa146.VerAuto();

        }
    }
}
