namespace DesignPatterns.Prototype.Core

{
    public class DSPrototype : AutoPrototype
    {
        public override AutoPrototype Clonar()
        {
            return (DSPrototype)this.MemberwiseClone();
        }

        public override string VerAuto()
        {
            return $"\n\nDS {_modelo} color {_color}";
        }
    }

}
