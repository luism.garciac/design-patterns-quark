using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DesignPatterns.Builder.Core
{
    public class BPizza
    {
        public Masa _masa;
        public Salsa _salsa;
        public Agregado _agregado;

        public string _tipo;

        public BPizza(Masa masa, Salsa salsa, Agregado agregado, string tipo)
        {
            _masa = masa;
            _salsa = salsa;
            _agregado = agregado;
            _tipo = tipo;
        }
    }

}

