﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Builder.Core
{
    class PizzaLightBuilder : PizzaBuilder
    {
        public PizzaLightBuilder()
        {
            _descripcion = "Pizza Light";
        }

        public override Agregado BuildAgregado()
        {
            return new Berenjena();
        }

        public override Masa BuildMasa()
        {
            return new MasaIntegral();
        }

        public override Salsa BuildSalsa()
        {
            return new SalsaLight();
        }
    }
}
