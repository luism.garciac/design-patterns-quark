﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Builder.Core
{
    public abstract class Salsa
    {
        protected string _descripcion;

        public string Descripcion { get => _descripcion; }
    }

    public class SalsaDeTomate : Salsa
    {
        public SalsaDeTomate()
        {
            _descripcion = "Salsa de tomate";
        }
    }

    public class SalsaDeOliva : Salsa
    {
        public SalsaDeOliva()
        {
            _descripcion = "Salsa de oliva";
        }
    }

    public class SalsaLight : Salsa
    {
        public SalsaLight()
        {
            _descripcion = "Salsa Light";
        }
    }
}
