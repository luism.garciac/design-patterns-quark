﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Builder.Core
{
    public abstract class Masa
    {
        protected string _descripcion;

        public string Descripcion { get => _descripcion;}
    }

    public class MasaAlMolde : Masa
    {
        public MasaAlMolde()
        {
            _descripcion = "Masa al Molde";
        }
    }

    public class MasaALaPiedra : Masa
    {
        public MasaALaPiedra()
        {
            _descripcion = "Masa a la Piedra";
        }
    }

    public class MasaIntegral : Masa
    {
        public MasaIntegral()
        {
            _descripcion = "Masa Integral";
        }
    }
}
