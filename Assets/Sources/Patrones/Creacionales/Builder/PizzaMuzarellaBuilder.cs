﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Builder.Core
{
    class PizzaMuzzarellaBuilder : PizzaBuilder
    {
        public PizzaMuzzarellaBuilder()
        {
            _descripcion = "Pizza Muzarella";
        }

        public override Agregado BuildAgregado()
        {
            return new Oregano();
        }

        public override Masa BuildMasa()
        {
            return new MasaAlMolde();
        }

        public override Salsa BuildSalsa()
        {
            return new SalsaDeTomate();
        }
    }
}
