﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace DesignPatterns.Builder.Core
{
    public class BuilderMain : MonoBehaviour
    {
        [Header("Builder Elements")]
        [SerializeField] private TMP_Dropdown _builderDropDown;
        [SerializeField] private TMP_Text _builderList;

        private List<PizzaBuilder> _lineas = new List<PizzaBuilder>();

        private void Start()
        {
            SetupBuilderExample();
        }

        private void SetupBuilderExample()
        {
            _lineas.Add(new PizzaItalianaBuilder());
            _lineas.Add(new PizzaLightBuilder());
            _lineas.Add(new PizzaMuzzarellaBuilder());
            _lineas.Add(new PizzaCanchaBuilder());

            List<string> descripciones = new List<string>();

            foreach (var item in _lineas)
            {
                descripciones.Add(item._descripcion);
            }

            _builderDropDown.ClearOptions();
            _builderDropDown.AddOptions(descripciones);
        }

        public void RunBuilderExample()
        {
            PizzaBuilder builder = FindBuilder(_builderDropDown.options[_builderDropDown.value].text);
            BPizza pizza = builder.BuildPizza();

            string descripcion = $"{pizza._tipo}. Masa: {pizza._masa.Descripcion}. Salsa: {pizza._salsa.Descripcion}. Agregado: {pizza._agregado.Descripcion}";

            _builderList.text = _builderList.text + $"\n\n{descripcion}";
        }

        private PizzaBuilder FindBuilder(string selectedItem)
        {
            foreach (var linea in _lineas)
            {
                if (linea._descripcion == selectedItem)
                    return linea;
            }

            return null;
        }

    }
}
