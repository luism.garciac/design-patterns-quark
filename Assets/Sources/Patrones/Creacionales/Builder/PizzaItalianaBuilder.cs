﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Builder.Core
{
    class PizzaItalianaBuilder : PizzaBuilder
    {
        public PizzaItalianaBuilder()
        {
            _descripcion = "Pizza Italiana";
        }

        public override Agregado BuildAgregado()
        {
            return new Anchoas();
        }

        public override Masa BuildMasa()
        {
            return new MasaALaPiedra();
        }

        public override Salsa BuildSalsa()
        {
            return new SalsaDeOliva();
        }
    }
}
