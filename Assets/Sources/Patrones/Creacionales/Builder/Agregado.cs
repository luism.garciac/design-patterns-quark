﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Builder.Core
{
    public abstract class Agregado
    {
        protected string _descripcion;

        public string Descripcion { get => _descripcion; }
    }

    public class Oregano : Agregado
    {
        public Oregano()
        {
            _descripcion = "Oregano fresco";
        }
    }

    public class Anchoas : Agregado
    {
        public Anchoas()
        {
            _descripcion = "Anchoas al aceite";
        }
    }

    public class Berenjena : Agregado
    {
        public Berenjena()
        {
            _descripcion = "Berenjena asada";
        }
    }
}
