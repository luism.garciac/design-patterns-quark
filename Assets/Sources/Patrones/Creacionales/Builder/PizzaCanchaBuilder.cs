﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Builder.Core
{
    class PizzaCanchaBuilder : PizzaBuilder
    {
        public PizzaCanchaBuilder()
        {
            _descripcion = "Pizza de Cancha";
        }

        public override Agregado BuildAgregado()
        {
            return new Anchoas();
        }

        public override Masa BuildMasa()
        {
            return new MasaIntegral();
        }

        public override Salsa BuildSalsa()
        {
            return new SalsaDeOliva();
        }
    }
}
