﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Facade.Core
{
    // Algunas Fachadas pueden trabajar con múltiples subsistemas al mismo tiempo.

    public class Subsystem2
    {
        public string InitSubsystem()
        {
            return "Subsistema 2 inicializado!\n";
        }

        public string RunOperationY()
        {
            return "Subsistema 2, operacion Y ejecutada!\n";
        }
    }
}
