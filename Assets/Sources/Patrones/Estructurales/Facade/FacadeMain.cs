﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace DesignPatterns.Facade.Core
{
    public class FacadeMain : MonoBehaviour
    {
        [SerializeField] private TMP_Text _console;

        public void RunFacadeExample()
        {
            _console.text = string.Empty;

            Subsystem1 subsystem1 = new Subsystem1();
            Subsystem2 subsystem2 = new Subsystem2();

            Facade facade = new Facade(subsystem1, subsystem2);

            _console.text += facade.Operation();
        }
    }
}
