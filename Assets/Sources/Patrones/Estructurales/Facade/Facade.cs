﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Facade.Core
{
    public class Facade
    {
        protected Subsystem1 _subsystem1;

        protected Subsystem2 _subsystem2;

        public Facade(Subsystem1 subsystem1, Subsystem2 subsystem2)
        {
            this._subsystem1 = subsystem1;
            this._subsystem2 = subsystem2;
        }

        public string Operation()
        {
            string result = "La Fachada inicializa los subsistemas:\n";
            result += this._subsystem1.InitSubsystem();
            result += this._subsystem2.InitSubsystem();

            result += "La fachada ejecuta las operaciones en los subsistemas\n";
            result += this._subsystem1.RunOperationX();
            result += this._subsystem2.RunOperationY();
            return result;
        }
    }
}
