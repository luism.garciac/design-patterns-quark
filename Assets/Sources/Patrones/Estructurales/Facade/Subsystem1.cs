﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Facade.Core
{
    //  Los subsistemas pueden aceptar solicitudes tanto de la Fachada o del cliente directamente.
    //  De cualquier manera, para los subsistemas, la Fachada es un cliente más y no es parte del subsistema.

    public class Subsystem1
    {
        public string InitSubsystem()
        {
            return "Subsistema 1 inicializado!\n";
        }

        public string RunOperationX()
        {
            return "Subsistema 1, Operacion X ejecutada!\n";
        }
    }
}
