﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Composite.Core
{
    public abstract class Componente
    {
        private string _nombre;

        public Componente(string nombre)
        {
            _nombre = nombre;
        }

        public string Nombre 
        { 
            get => _nombre;
        }

        public abstract void AgregarHijo(Componente c);
        public abstract IList<Componente> ObtenerHijos();
        public abstract int ObtenerTamaño { get; }

    }
}
