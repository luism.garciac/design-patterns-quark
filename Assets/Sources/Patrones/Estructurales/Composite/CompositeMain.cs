﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace DesignPatterns.Composite.Core
{
    public class CompositeMain : MonoBehaviour
    {
        [SerializeField] private TMP_Text _console;

        public void RunCompositeExample()
        {
            _console.text = string.Empty;

            Componente root = new Directorio("raiz");

            Componente archivo1 = new Archivo("archivo1.txt", 25);
            Componente archivo2 = new Archivo("archivo2.txt", 78);
            Componente archivo3 = new Archivo("archivo3.txt", 15287);
            Componente archivo4 = new Archivo("archivo4.txt", 1241);
            Componente archivo5 = new Archivo("archivo5.txt", 838);
            Componente archivo6 = new Archivo("archivo6.txt", 500);
            Componente archivo7 = new Archivo("archivo7.txt", 100);

            Componente dir1 = new Directorio("dir1");
            Componente dir2 = new Directorio("dir2");
            Componente dir3 = new Directorio("dir3");

            dir1.AgregarHijo(archivo1);
            dir2.AgregarHijo(archivo2);
            dir3.AgregarHijo(archivo3);
            dir3.AgregarHijo(archivo4);
            dir1.AgregarHijo(dir3);
            dir1.AgregarHijo(archivo6);
            dir2.AgregarHijo(archivo7);

            root.AgregarHijo(dir1);
            root.AgregarHijo(dir2);
            root.AgregarHijo(archivo5);

            _console.text += ($"\n\nEl tamanio del directorio {root.Nombre} es {root.ObtenerTamaño}");
            _console.text += ($"\n\nEl tamanio del directorio {dir1.Nombre} es {dir1.ObtenerTamaño}");
            _console.text += ($"\n\nEl tamanio del directorio {dir2.Nombre} es {dir2.ObtenerTamaño}");
            _console.text += ($"\n\nEl tamanio del directorio {dir3.Nombre} es {dir3.ObtenerTamaño}");
        }
    }
}
