﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Composite.Core
{
    public class Archivo : Componente
    {
        int _tamanio;

        public Archivo(string nombre, int tamanio) : base(nombre)
        {
            _tamanio = tamanio;
        }

        public int Tamanio 
        { 
            get => _tamanio;
        }

        public override int ObtenerTamaño
        {
            get 
            {
                return _tamanio;
            }
        }


        public override void AgregarHijo(Componente c)
        {

        }

        public override IList<Componente> ObtenerHijos()
        {
            return null;
        }
    }
}
