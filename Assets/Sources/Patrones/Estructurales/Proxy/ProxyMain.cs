﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace DesignPatterns.Proxy.Core
{
    public class ProxyMain : MonoBehaviour
    {
        [SerializeField] private TMP_Text _console;

        public void RunProxyExample()
        {

            _console.text = string.Empty;

            try
            {
                IRepository repo = new CustomerRepositoryProxy();

                Customer p1 = new Customer("Juan Lopez");
                Customer p2 = new Customer("Jose Paz");

                Session.CanGetAll = true;
                Session.CanSave = true;

                repo.Save(p1);
                repo.Save(p2);

                foreach (var p in repo.GetAll())
                {
                    _console.text += $"\n\n{p.Name}";
                }

            }
            catch (System.Exception)
            {
                throw;
            }
        }
    }
}
