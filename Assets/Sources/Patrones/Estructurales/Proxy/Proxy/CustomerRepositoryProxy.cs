﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Proxy.Core
{
    public class CustomerRepositoryProxy : IRepository
    {
        IRepository _real;

        public IList<Customer> GetAll()
        {
            if (_real == null) _real = new CustomerRepository(); //No tendremos referencia hasta la primera invocación

            if (Session.CanGetAll)
            {
                return _real.GetAll();
            }
            else
            {
                throw new UnauthorizedAccessException();
            }
        }

        public void Save(Customer customer)
        {
            if (_real == null) _real = new CustomerRepository();

            if (Session.CanSave)
            {
                _real.Save(customer);
            }
            else
            {
                throw new UnauthorizedAccessException();
            }
        }
    }
}
