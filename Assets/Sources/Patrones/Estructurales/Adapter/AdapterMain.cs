﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace DesignPatterns.Adapter.Core
{
    public class AdapterMain : MonoBehaviour
    {
        [SerializeField] private TMP_Text _console;

        public void RunAdapterExample()
        {
            _console.text = string.Empty;

            Motor motor1 = new MotorNaftero();
            _console.text += motor1.Arrancar();
            _console.text += motor1.Acelerar();
            _console.text += motor1.Detener();
            _console.text += motor1.CargarCombustible();

            Motor motor2 = new MotorDiesel();
            _console.text += motor2.Arrancar();
            _console.text += motor2.Acelerar();
            _console.text += motor2.Detener();
            _console.text += motor2.CargarCombustible();

            Motor motor3 = new MotorElectricoAdapter();
            _console.text += motor3.Arrancar();
            _console.text += motor3.Acelerar();
            _console.text += motor3.Detener();
            _console.text += motor3.CargarCombustible();
        }
    }
}
