﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace DesignPatterns.Adapter.Core
{
    public class MotorNaftero : Motor
    {
        public override string Acelerar()
        {
            return("\n\nAcelerando motor naftero...");
        }

        public override string Detener()
        {
            return("\n\nDeteniendo motor naftero...");

        }

        public override string Arrancar()
        {
            return ("\n\nArrancando motor naftero...");
        }

        public override string CargarCombustible()
        {
            return ("\n\nCargando combustible al motor naftero...");
        }
    }
}
