﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Adapter.Core
{
    public abstract class Motor
    {
        public abstract string Acelerar();
        public abstract string Arrancar();
        public abstract string Detener();
        public abstract string CargarCombustible();
    }
}
