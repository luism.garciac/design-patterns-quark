﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace DesignPatterns.Adapter.Core
{
    public class MotorDiesel : Motor
    {
        public override string Acelerar()
        {
            return("\n\nAcelerando motor diesel...");
        }

        public override string Detener()
        {
            return("\n\nDeteniendo motor diesel...");

        }

        public override string Arrancar()
        {
            return("\n\nArrancando motor diesel...");
        }

        public override string CargarCombustible()
        {
            return("\n\nCargando combustible al motor diesel...");
        }
    }
}
