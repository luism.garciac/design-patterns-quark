﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace DesignPatterns.Adapter.Core
{
    public class MotorElectricoAdapter : Motor
    {
        MotorElectrico _motorElectrico = new MotorElectrico();

        public override string Acelerar()
        {
            return _motorElectrico.Mover();
        }

        public override string Arrancar()
        {
            return $"{_motorElectrico.Conectar()} {_motorElectrico.Activar()}";
        }

        public override string Detener()
        {
            return $"{_motorElectrico.Parar()} {_motorElectrico.Desactivar()}";
        }

        public override string CargarCombustible()
        {
            return _motorElectrico.Enchufar();
        }
    }
}
