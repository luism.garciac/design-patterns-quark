﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace DesignPatterns.Adapter.Core
{
    public class MotorElectrico
    {
        private bool _conectado;
        private bool _activo;
        private bool _moviendo;

        public string Conectar()
        {
            if (_conectado)
            {
                return ("\n\nImposible conectar motor electrico ya conectado!");
            }
            else
            {
                _conectado = true;
                return ("\n\nMotor electrico conectado!");
            }
        }

        public string Activar()
        {
            if (!_conectado)
            {
                return("\n\nImposible activar un motor electrico no conectado!");
            }
            else
            {
                _activo = true;
                return ("\n\nMotor activado!");
            }
        }
        public string Mover()
        {
            if (_conectado && _activo)
            {
                _moviendo = true;
                return ("\n\nMoviendo vehiculo con motor electrico!");
            }
            else
            {
                _activo = true;
                return ("\n\nEl motor debe estar conectado y activo!");
            }
        }
        public string Parar()
        {
            _moviendo = false;
            return ("\n\nMotor electrico detenido!");

        }
        public string Desconectar()
        {
            return null;
        }
        public string Desactivar()
        {
            _activo = false;
            return ("\n\nMotor electrico desconectado!");
        }

        public string Enchufar()
        {
            if (!_activo)
            {
                _activo = false;
                return ("\n\nMotor cargando las baterias!");
            }
            else
            {
                return ("\n\nImposible enchufar un motor activo!");
            }
        }

    }
}
