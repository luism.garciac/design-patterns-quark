﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace DesignPatterns.Flyweight.Core
{
    public class FlyweightMain : MonoBehaviour
    {
        [SerializeField] private TMP_Text _console;

        public void RunFlyweightExample()
        {
            _console.text = string.Empty;

            //  El código de cliente generalmente crea un grupo de Flyweights pre-cargados en el estado de inicialización de la aplicación.

            var factory = new FlyweightFactory(
                new Car { Company = "Chevrolet", Model = "Camaro2018", Color = "rosa" },
                new Car { Company = "Mercedes Benz", Model = "C300", Color = "negro" },
                new Car { Company = "Mercedes Benz", Model = "C500", Color = "rojo" },
                new Car { Company = "BMW", Model = "M5", Color = "rojo" },
                new Car { Company = "BMW", Model = "X6", Color = "blanco" }
            );

            _console.text += factory.ListFlyweights();

            AddCarToPoliceDatabase(factory, new Car
            {
                Number = "AK451IR", Owner = "Juan Perez", Company = "BMW", Model = "M5", Color = "rojo"
            });

            AddCarToPoliceDatabase(factory, new Car
            {
                Number = "CL234IR", Owner = "Jose Juarez", Company = "BMW", Model = "X1", Color = "rojo"
            });

            AddCarToPoliceDatabase(factory, new Car
            {
                Number = "BB188EI", Owner = "Carmen Pipolo", Company = "Chevrolet", Model = "Camaro2018", Color = "verde"
            });

            AddCarToPoliceDatabase(factory, new Car
            {
                Number = "JE933GI", Owner = "Agustina Gomez", Company = "Mercedes Benz", Model = "C300", Color = "negro"
            });

            _console.text += factory.ListFlyweights();
        }

        public void AddCarToPoliceDatabase(FlyweightFactory factory, Car car)
        {
            string log = string.Empty;

            log += $"\nCliente: Agregando auto a la base de datos.";

            (string flyWeightLog, Flyweight flyweight) = factory.GetFlyweight(new Car
            {
                Color = car.Color,
                Model = car.Model,
                Company = car.Company
            });

            log += flyWeightLog;

            //  El código de cliente o bien almacena o calcula el estado extrínseco y lo pasa a los métodos del Flyweight.

            _console.text += log;
            _console.text += flyweight.Operation(car);
        }
    }
}
