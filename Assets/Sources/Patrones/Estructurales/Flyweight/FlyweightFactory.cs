﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace DesignPatterns.Flyweight.Core
{
    public class FlyweightFactory
    {
        private List<Tuple<Flyweight, string>> flyweights = new List<Tuple<Flyweight, string>>();

        public FlyweightFactory(params Car[] args)
        {
            foreach (var elem in args)
            {
                flyweights.Add(new Tuple<Flyweight, string>(new Flyweight(elem), this.GetKey(elem)));
            }
        }

        //  Devuelve un hash de string para un estado dado.
        public string GetKey(Car key)
        {
            List<string> elements = new List<string>();

            elements.Add(key.Model);
            elements.Add(key.Color);
            elements.Add(key.Company);

            if (key.Owner != null && key.Number != null)
            {
                elements.Add(key.Number);
                elements.Add(key.Owner);
            }

            return string.Join("_", elements);
        }

        //  Devuelve un Flyweight existente con un estado dado, o crea uno nuevo.

        public (string, Flyweight) GetFlyweight(Car sharedState)
        {
            string key = this.GetKey(sharedState);
            string log = string.Empty;

            if (flyweights.Where(t => t.Item2 == key).Count() == 0)
            {
                log = "\nFabrica de Flyweights: Flyweight no encontrado. Se crea un nuevo Flyweight.";
                this.flyweights.Add(new Tuple<Flyweight, string>(new Flyweight(sharedState), key));
            }
            else
            {
                log = "\nFabrica de Flyweights: Reutilizando Flyweight existente.";
            }

            return (log, this.flyweights.Where(t => t.Item2 == key).FirstOrDefault().Item1);
        }

        public string ListFlyweights()
        {
            string log = string.Empty;

            var count = flyweights.Count;
            log = $"\nFabrica de Flyweights: Tengo {count} Flyweights:\n";

            foreach (var flyweight in flyweights)
            {
                log += $"\n{flyweight.Item2}";
            }

            return $"{log}\n";
        }
    }
}
