﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Bridge.Core
{
    public class RemoteControl
    {
        protected Device _device;

        public RemoteControl(Device device)
        {
            _device = device;
        }

        public string TogglePower()
        {
            if (_device.IsEnabled())
            {
                _device.Disable();
                return $"\nEl dispositivo {_device.GetName()} ha sido apagado";
            }
            else
            {
                _device.Enable();
                return $"\nEl dispositivo {_device.GetName()} ha sido encendido";
            }
        }

        public string VolumeDown()
        {
            if (!_device.IsEnabled())
                return $"\nEl dispositivo {_device.GetName()} esta apagado";

            _device.SetVolume(_device.GetVolume() - 10);
            return $"\nEl volumen es: {_device.GetVolume()}";
        }

        public string VolumeUp()
        {
            if (!_device.IsEnabled())
                return $"\nEl dispositivo {_device.GetName()} esta apagado";

            _device.SetVolume(_device.GetVolume() + 10);
            return $"\nEl volumen es: {_device.GetVolume()}";
        }

        public string ChannelUp()
        {
            if (!_device.IsEnabled())
                return $"\nEl dispositivo {_device.GetName()} esta apagado";

            _device.SetChannel(_device.GetChannel() + 1);
            return $"\nEl canal es: {_device.GetChannel()}";
        }

        public string ChannelDown()
        {
            if (!_device.IsEnabled())
                return $"\nEl dispositivo {_device.GetName()} esta apagado";

            _device.SetChannel(_device.GetChannel() - 1);
            return $"\nEl canal es: {_device.GetChannel()}";
        }

    }
}
