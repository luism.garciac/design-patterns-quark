using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace DesignPatterns.Bridge.Core
{
    public class BridgeMain : MonoBehaviour
    {
        [SerializeField] private TMP_Text _console;

        public void RunBridgeExample()
        {
            Television television = new Television(152, 40, false);
            Radio radio = new Radio(1350, 60, false);

            RemoteControl remoteTV = new RemoteControl(television);
            _console.text += remoteTV.ChannelDown();
            _console.text += remoteTV.TogglePower();
            _console.text += remoteTV.ChannelUp();
            _console.text += remoteTV.VolumeUp();

            RemoteControl remoteRadio = new RemoteControl(radio);
            _console.text += remoteRadio.TogglePower();
            _console.text += remoteRadio.VolumeDown();
            _console.text += remoteRadio.ChannelUp();
            _console.text += remoteRadio.TogglePower();
        }
    }
}
