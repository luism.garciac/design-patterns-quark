﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Bridge.Core
{
    public class Television : Device
    {
        private int _channel;
        private int _volume;
        private bool _enabled;

        public Television(int channel, int volume, bool enabled)
        {
            _channel = channel;
            _volume = volume;
            _enabled = enabled;
        }

        public void Disable()
        {
            _enabled = false;
        }

        public void Enable()
        {
            _enabled = true;
        }

        public int GetChannel()
        {
            return _channel;
        }

        public int GetVolume()
        {
            return _volume;
        }

        public bool IsEnabled()
        {
            return _enabled;
        }

        public void SetChannel(int channel)
        {
            _channel = channel;
        }

        public void SetVolume(int volume)
        {
            _volume = volume;
        }

        public string GetName()
        {
            return "Television";
        }
    }
}
