﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Bridge.Core
{
    public interface Device
    {
        public bool IsEnabled();
        public void Enable();
        public void Disable();
        public int GetVolume();
        public void SetVolume(int volume);
        public int GetChannel();
        public void SetChannel(int channel);
        public string GetName();
    }
}
