﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace DesignPatterns.Decorator.Core
{
    public class DecoratorMain : MonoBehaviour
    {
        [SerializeField] private TMP_Text _console;

        public void RunDecoratorExample()
        {
            _console.text = string.Empty;

            BebidaComponent bebida1 = new CafeSolo();

            bebida1 = new Leche(bebida1);
            bebida1 = new Azucar(bebida1);
            bebida1 = new Azucar(bebida1);

            _console.text += $"\n\nProducto: {bebida1.Descripcion} tiene un costo de: ${bebida1.Costo}";


            BebidaComponent bebida2 = new CafeDescafeinado();

            bebida2 = new Azucar(bebida2);
            bebida2 = new Crema(bebida2);

            _console.text += $"\n\nProducto: {bebida2.Descripcion} tiene un costo de: ${bebida2.Costo}";


            BebidaComponent bebida3 = new TeTradicional();

            bebida3 = new Azucar(bebida3);
            bebida3 = new Azucar(bebida3);
            bebida3 = new Azucar(bebida3);

            _console.text += $"\n\nProducto: {bebida3.Descripcion} tiene un costo de: ${bebida3.Costo}";


            BebidaComponent bebida4 = new CafeExpresso();

            bebida4 = new Edulcorante(bebida4);

            _console.text += $"\n\nProducto: {bebida4.Descripcion} tiene un costo de: ${bebida4.Costo}";
        }
    }
}
