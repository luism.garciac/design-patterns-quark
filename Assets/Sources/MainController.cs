using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainController : MonoBehaviour
{
    #region InitialSetups

    [SerializeField] private DesignPatterData _designPatternsData;
    [SerializeField] private PatternTypes _designPatternTypeDesc;
    [SerializeField] private SolidPrinciples _solidPrinciplesDesc;

    [SerializeField] private GameObject _patternDiagramGO;
    [SerializeField] private GameObject _patternExampleDiagramGO;
    [SerializeField] private GameObject _patternDescriptionGO;
    [SerializeField] private GameObject _patternExampleGO;
    [SerializeField] private GameObject _patternClientCodeGO;

    [SerializeField] private TMP_Text _patternTitle;
    [SerializeField] private Image _patternDiagram;
    [SerializeField] private Image _patternExampleDiagram;
    [SerializeField] private Image _patternClientCode;
    [SerializeField] private TMP_Text _patternDescription;

    [SerializeField] private TMP_Text _patternTypeInfo;

    private GameObject _exampleInstance;


    private void Start()
    {
        SetupExample("Bienvenido");
    }

    #endregion

    #region ApplicationMethods

    public void SetupExample(string patternName)
    {
        if (_exampleInstance != null)
            Destroy(_exampleInstance);

        foreach (var pattern in _designPatternsData._designPatterns)
        {
            if (pattern._title == patternName)
            {
                _patternTitle.text = pattern._title;
                _patternDescription.text = pattern._description;
                _patternDiagram.sprite = pattern._UMLDiagram;
                _patternExampleDiagram.sprite = pattern._UMLDiagramExample;
                _patternClientCode.sprite = pattern._clientCode;

                if (pattern._example != null)
                    _exampleInstance = Instantiate(pattern._example, _patternExampleGO.transform);
            }
        }

        _patternDescriptionGO.SetActive(true);
        _patternExampleDiagramGO.SetActive(false);
        _patternClientCodeGO.SetActive(false);
        _patternDiagramGO.SetActive(false);
        _patternExampleGO.SetActive(false);
    }

    public void SwitchSection(GameObject section)
    {
        DisableAllSections();
        section.SetActive(true);
    }

    public void SetPatternInfo(string patternType)
    {
        foreach (var patternTypeDesc in _designPatternTypeDesc._designPatternsTypeDesc)
        {
            if (patternTypeDesc._title == patternType)
            {
                _patternTypeInfo.text = patternTypeDesc._description;
            }
        }
    }

    public void DisableAllSections()
    {
        _patternDescriptionGO.SetActive(false);
        _patternExampleDiagramGO.SetActive(false);
        _patternDiagramGO.SetActive(false);
        _patternExampleGO.SetActive(false);
        _patternClientCodeGO.SetActive(false);
    }

    public void ShowSolid(string solid)
    {
        if (_exampleInstance != null)
            Destroy(_exampleInstance);

        foreach (var solidDesc in _solidPrinciplesDesc._solidPrinciplesDesc)
        {
            if (solidDesc._title == solid)
            {
                _patternTitle.text = solidDesc._title;
                _patternDescription.text = solidDesc._description;
                _patternDiagram.sprite = null;
                _patternExampleDiagram.sprite = null;
                _patternClientCode.sprite = null;
            }
        }

        _patternDescriptionGO.SetActive(true);
        _patternExampleDiagramGO.SetActive(false);
        _patternDiagramGO.SetActive(false);
        _patternExampleGO.SetActive(false);
        _patternClientCodeGO.SetActive(false);
    }

    public void OnCloseAppButtonClick()
    {
        Application.Quit();
    }

    #endregion

}
