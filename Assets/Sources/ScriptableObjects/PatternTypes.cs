﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "DesignPatternTypeDesc", menuName = "DesignPatternTypeDesc", order = 1)]
public class PatternTypes : ScriptableObject
{
    public List<DesignPatternSingleTypeDesc> _designPatternsTypeDesc;

    [System.Serializable]
    public class DesignPatternSingleTypeDesc
    {
        public string _title;
        [TextArea(10, 100)]
        public string _description;
    }
}