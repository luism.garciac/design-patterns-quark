using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DesignPatternDataList", menuName = "DesignPatternDataList", order = 1)]
public class DesignPatterData : ScriptableObject
{
    public List<DesignPatternSingleData>  _designPatterns;

    [System.Serializable]
    public class DesignPatternSingleData
    {
        public string _title;

        [TextArea(10, 100)]
        public string _description;

        public Sprite _UMLDiagram;
        public Sprite _UMLDiagramExample;
        public Sprite _clientCode;
        public GameObject _example;
    }

}
