﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "SolidPrinciplesDesc", menuName = "SolidPrinciplesDesc", order = 1)]
public class SolidPrinciples : ScriptableObject
{
    public List<SolidPrinciplesDesc> _solidPrinciplesDesc;

    [System.Serializable]
    public class SolidPrinciplesDesc
    {
        public string _title;
        [TextArea(10, 100)]
        public string _description;
    }
}